+++
date = "2015-07-28"
weight = 100

title = "Content hand-over Related"

outputs = ["html", "pdf-in"]

aliases = [
    "/old-wiki/Content_hand-over/Related"
]

status = "Requires Update"
statusDescription = "Fold into main content hand-over concept to make it self contained."
+++

The following use-cases are superficially similar to [content
hand-over]( {{< ref "/concepts/content_hand-over.md" >}} ), but have somewhat different
requirements. We recommend that they are examined separately.

## Use-cases examined on separate pages

  - [Interface discovery]( {{< ref "/concepts/interface_discovery.md" >}} )
  - [Sharing]( {{< ref "/concepts/sharing.md" >}} )
  - [Points of interest]( {{< ref "/points_of_interest.md" >}} )

## Monitoring status

An application author wishes to indicate whether calling and/or SMS are
currently available. ()

  - The platform must make this information available.
      - *We recommend implementing this by making it available as a C
        API in the platform, perhaps implemented in terms of oFono's
        D-Bus API or an intermediary service such as Beckfoot. We
        recommend treating this as unrelated to content handover.*
      - *This feature needs a security model: is it available to all
        apps, or only to apps with a particular privilege listed in
        their manifests?*

An application author might be interested in the status of features of
another application bundle. ()

  - *If this is required, a clear security model is needed, and should
    be documented. Otherwise there is a risk of unintended cross-app
    data disclosure.*
  - *We recommend that this should not be considered unless specific
    use-cases can be found. If it is required, we recommend not
    including this feature in the scope of content handover. As a
    general design point, we recommend avoiding premature
    generalization; accordingly, we suggest designing this facility on a
    case-by-case basis (perhaps by having the status-advertising
    application export its own D-Bus API with signals and properties),
    and only considering a more general SDK API if there are several
    use-cases with notably similar requirements.*
