+++
date = "2018-10-03"
weight = 100

title = "image-x-start"

aliases = [
    "/old-wiki/QA/Test_Cases/image-x-start"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
