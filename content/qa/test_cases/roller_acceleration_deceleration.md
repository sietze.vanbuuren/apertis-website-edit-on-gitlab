+++
date = "2017-10-27"
weight = 100

title = "roller_acceleration_deceleration"

aliases = [
    "/old-wiki/QA/Test_Cases/roller_acceleration_deceleration"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
