+++
date = "2020-06-10"
weight = 100

title = "Intel 64-bit Reference Hardware"
+++

The recommended Intel 64-bit
[reference hardware]( {{< ref "/reference_hardware/_index.md" >}} )
is the Minnowboard Turbot. Please see its
[setup guide]( {{< ref "/reference_hardware/minnowboard_setup.md" >}} )
for first-time setup.
The following [optional extras]( {{< ref "/reference_hardware/extras.md" >}} )
may be of interest.

If you currently don't have access to any supported hardware, the `amd64`
images can be run on a
[virtual machine]( {{< ref "/guides/virtualbox.md" >}} ).

| Reference                              | Hardware                                                                                                       | Comments                                                                                                                                                                                                                                             |
| -------------------------------------- | -------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Intel reference device                 | MinnowBoard Turbot Dual-Core (E3826)                                                                           | [Setup guide]( {{< ref "/reference_hardware/minnowboard_setup.md" >}} ) The system does not come with a power-supply, be sure to order one separately                                                                                                        |
| Alternative Intel reference device     | MinnowBoard Turbot Quad-Core (E3845)                                                                           | **Untested**                                                                                                                                                                                                                                         |
| MicroSD card                           | 16GB or larger microSD card                                                                                    | Speed class C10/U1 or better recommended                                                                                                                                                                                                             |
| Optional: Multitouch screen            | [Lilliput FA1014-NP/C/T](http://www.lilliputdirect.com/lilliput-fa1014-10-inch-capacitive-touchscreen-monitor) | Only needed for developers of multitouch applications or working with the multitouch stack. Other developers can just use a HDMI monitor. Monitor does not ship with a stand - needs to be bought separately Monitor also functions as audio output. |
