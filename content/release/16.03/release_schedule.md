+++
date = "2016-06-07"
weight = 100

title = "16.03 Release schedule"

aliases = [
    "/16.03/releaseschedule.md",
    "/old-wiki/16.03/ReleaseSchedule",
    "/old-wiki/16.03/Release_schedule"
]
+++

The 16.03 release cycle started in 2016-01. [You can follow the
countdown on Phabricator](https://phabricator.apertis.org/countdown/).

| Event                  | Date       |
| ---------------------- | ---------- |
| Soft freeze            | 2016-02-24 |
| Hard freeze            | 2016-03-02 |
| Release candidate 1    | 2016-03-04 |
| 16.03 release          | 2016-03-18 |
| 16.03 security updates | 2016-07-22 |