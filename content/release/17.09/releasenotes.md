+++
date = "2017-12-21"
weight = 100

title = "17.09 ReleaseNotes"

aliases = [
    "/old-wiki/17.09/ReleaseNotes"
]
status = "Deprecated"
statusDescription = "This release of Apertis has been deprecated. Links to images and other resources have either been removed or are likely to be dead."
+++

# Apertis 17.09 Release

**17.09** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (both in the 32bit ARMv7 version using
the hardfloat ABI and in the 64-bit ARMv8 version) and Intel x86-64
(64-bit) systems.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customization. Test results of the 17.09 release
are available in the [17.09 test report]().

### Release downloads

| [Apertis 17.09 images]() |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |

The `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 17.09 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` 17.09 target helper-libs development sdk hmi`

## New features

During this release the focus has been on bugfixes and improvements on
the server infrastructure, but this does not mean that we lack new
features: in particular, the application framework refactoring that has
been started during the 17.06 cycle has landed in 17.09, and it is now
able to work on system which lack direct access to a Btrfs filesystem.
This is extremely useful for systems using flash-based filesystems and
for containers-based setups where allowing the container privileged
access to the file system may be undesirable. Compatibility with
existing Btrfs-based setups is preserved, and they can be migrated
transparently.

## Groundwork

Apertis [now works better in a container]( ) right
out of the box, ensuring that AppArmor is enabled and disabling some
filesytem-related services which are not needed inside a container.

Automation has been a recurring theme during this cycle, and now the
[developers documentation](https://developer.apertis.org/) is
automatically deployed on push. The main automation effort has been in
defining more formally the whole release branching process and automate
the most of it: this will allow us to make releases with more ease,
reducing the chance of errors and focusing our efforts on making Apertis
even more awesome.

## Design and documentation

The [Apertis platform
guide]( {{< ref "platform-guide.md" >}} ) now provides
an high-level overview of what the Apertis platform is, which components
compose it and what are the concepts that drive its development.

The Hello World section in the
application developers guide has been updated to reflect the latest
Eclipse UI.

If you want to run Apertis in a container, the [setup
instructions]( ) have been overhauled and
clarified, and running an Apertis container under an Apertis system just
takes a couple of minutes.

On the design front, the [Inter-Domain Communications
document]( {{< ref "inter-domain-communication.md" >}} )
now has some provisions for sharing large amount of static data as bulk
transfers, such as firmware updates or map data for offline navigation.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

#### Mildenhall UI framework

<dl>

<dt>

The Mildenhall UI toolkit is deprecated in favour of other UI toolkits
such as GTK+, Qt, or HTML5-based solutions

<dd>

From the next release Apertis may no longer ship the Mildenhall UI
toolkit, as the underlying Clutter library has been deprecated upstream
for some years now.

</dl>

#### Application framework

<dl>

<dt>

`ribchester-full` and `canterbury-full` are deprecated

<dd>

From the next release Canterbury and Ribchester will only offer the APIs
provided by the respective `-core` and the additional APIs offered by
the `-full` packages will be dropped\<

</dl>

### Breaks

No known breaking changes are part of the 17.09 release.

## Infrastructure

### Apertis infrastructure tools

For Debian Jessie based systems:

` $ deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` $ deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

## Known issues

### High (3)

  - Crash observed on seed module when we accesing the D-Bus call method

  - Containers fail to load on Gen4 host

  - The video player window is split into 2 frames in default view

### Normal (132)

  - On first flashing of the image any second application does not get
    displayed (eg. album art)

  - On multiple re-entries from settings to eye the compositor hangs

  - Automated tests are not run on ARM

  - introspectable support for GObject property, signal and methods

  - Target ARM images contain a spurious rootfs.tar.gz in the boot
    partition

  - Crash observed on webruntime framework

  - Incorrect network available state shown in GNetworkMonitor

  - libreoffice fails to build from source on aarch64

  - Observing multiple service instances in all 17.06 SDK images

  - Frampton application doesn't load when we re-launch them after
    clicking the back button

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Render theme buttons are not updating with respect to different zoom
    levels

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Broken HTML generation in Backworth for the developers portal
    website

  - Page rendering is not smooth in sites like www.yahoo.com

  - Mildenhall should install themes in the standard xdg data dirs

  - webkit2GTK crash observed flicking on webview from other widget

  - Failed to load Captcha in Apertis developer portal

  - webkit-ac-3d-rendering test case fails

  - Newport test fails on minimal images

  - Avoid unconstrained dbus AppArmor rules in frome

  - Steps like pattern is seen in the background in songs application

  - virtual keyboard is not showing for password input field of any
    webpage

  - u-boot: Unable to find the device tree for R-car starter kit H3UCLB

  - Shutdown not working properly in virtualbox

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - Mismatching gvfs/gvfs-common and libatk1.0-0/libatk1.0-data package
    versions in the archive

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - libshoreham packaging bugs

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - In mildenhall, URL history speller implementation is incomplete.

  - Variable roller is not working

  - Roller problem in settings application

  - Songs do not start playing from the beginning but instead start a
    little ahead

  - Blank screen seen in Songs application on re-entry

  - Compositor hides the other screens

  - Status bar is not getting updated with the current song/video being
    played

  - canterbury: Most of the tests fail

  - ribchester: gnome-desktop-testing test times out

  - rhosydd: integration test fails

  - didcot-client: autopkgtest fails with
    org.apertis.Didcot.Error.NoApplicationFound and ""Unit
    didcot.service could not be found""

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - Cross debugging through GDB and GDBserver Not possible.

  - Voice/Audio is not heard for the Bluez-hfp profile in i.MX6

  - Deploying LAVA tests for SDK images on personal stream fails

  - The web runtime doesn't set the related view when opening new
    windows

  - Segmentation fault when disposing test executable of mildenhall

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - Album art is missing in one of the rows of the songs application

  - folks: random tests fail

  - GLib, GIO Reference Manual links are incorrectly swapped

  - GObject Generator link throws 404 error

  - Search feature doesn't work correctly for appdev portal located at
    `https://appdev.apertis.org/documentation/index.html`

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - gupnp-services tests test_service_browsing and
    test_service_introspection fail on target-arm image

  - Resizing the window causes page corruption

  - Content on a webpage doesn't load in sync with the scroll bar

  - Compositor seems to hide the bottom menu of a webpage

  - telepathy-gabble-tests should depend on python-dbus

  - libgles2-vivante-dev is not installable

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - apparmor-folks: unable to link contacts to test unlinking

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - Cannot open links within website like yahoo.com

  - mildenhall_launcher process needs to be killed in order to view
    browser using webkit2 GtkClutterLauncher

  - polkit-parsing: TEST_RESULT:fail

  - make check fails on libbredon package for wayland warnings

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - Canterbury messes up kerning when .desktop uses unicode chars

  - VirtualBox freezes using 100% CPU when resuming the host from
    suspend

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - apparmor-tracker: underlying_tests failed

  - tracker-indexing-local-storage: Stopping tracker-store services

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - Unusable header in Traprain section in Devhelp

  - Album Art Thumbnail missing in Thumb view in ARTIST Application

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - Test apps are failing in Liblightwood with the use of GTest

  - Focus in launcher rollers broken because of copy/paste errors

  - Mismatch between server version file and sysroot version

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Crash when initialising egl on ARM target

  - Zoom in feature does not work on google maps

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Drop down lists are not working on a site like facebook

  - gupnp-services: test service failed

  - LAVA: Could not resolve host: repositories.apertis.org

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - factory reset with a different image's rootfs.tar.gz results in
    emergency mode

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - Fix folks EDS tests to not be racy

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - Background video is not played in some website with
    GtkClutterLauncher

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - The background HMI is blank on clicking the button for Power OFF

  - libsoup-unit: ssl-test failed for ARM

  - Horizontal scroll is not shown on GtkClutterLauncher

  - telepathy-gabble: Several tests failed

  - Investigate why development ARM image fails to generate

  - minimal image: DISPLAY sanity check tests failed

  - Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - Back option is missing as part of the tool tip

  - Target doesn't reboot after system update

  - Bluetooth pairing option not working as expected

  - Video doesn't play when toggling from full screen to detail view

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - cgroups-resource-control: blkio-weights tests failed

  - Videos are hidden when Eye is launched

  - mildenhall-settings: does not generate localization files from
    source

  - libgrassmoor: executes tracker-control binary

  - Clutter_text_set_text API redraws entire clutterstage

  - factory-reset-tool TC: flagcheck messages are hidden by Plymouth

  - mxkineticscrollview-smooth-pan:Free scroll doesn't work

  - Network search pop-up isn't coming up in wi-fi settings

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - apparmor-pulseaudio: ARM Failed to drain stream: Timeout

  - Power button appers to be disabled on target

  - Documentation is not available from the main folder

  - Only half of the hmi is covered when opening gnome.org

  - remove INSTALL, aclocal.m4 files from langtoft

  - No connectivity Popup is not seen when the internet is disconnected.

  - Not able to load heavy sites on GtkClutterLauncher

### Low (16)

  - developer.a.o CSS broken

  - connman: patch "device: Don't report EALREADY" not accepted upstream

  - connman: patch "Use ProtectSystem=true" rejected upstream

  - qemu-arm-static not found in the pre installed qemu-user-static
    package

  - Spacing issues between text and selection box in website like amazon

  - LAVA doesn't report all results for some Target ARM tests

  - gstreamer1-0-decode: Failed to load plugin warning

  - beep audio decoder gives errors continously

  - Context drawer displced when switching to fullscreen in browser app

  - Printing hotdoc webpage directly results in misformatted document

  - Simulator screen is not in center but left aligned

  - bluetooth device pair failing with ""Invalid arguments in method
    call""

  - webkit-clutter-javascriptcore: run-javascriptcore-tests fails

  - Mildenhall compositor crops windows

  - Upstream: linux-tools-generic should depend on lsb-release

  - Remove unnecessary folks package dependencies for automated tests
