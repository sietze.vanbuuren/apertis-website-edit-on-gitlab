+++
date = "2020-09-10"
weight = 100

title = "v2020.3 Release Schedule"
+++

The v2020.3 release cycle started in October 2020.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2020-10-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2020-11-13        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2020-11-20        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2020-11-26        |
| RC testing                                                                                               | 2020-11-27..12-02 |
| v2020.3 release                                                                                          | 2020-12-03        |

## See also

  - Previous [release schedules]( {{< ref "releases.md" >}} ) and more
    information about the timeline
