+++
date = "2022-03-24"
weight = 100

title = "v2023dev2 Release schedule"
+++

The v2023dev2 release cycle started in April 2022.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2022-04-01        |
| Soft feature freeze: end of feature proposal and review period                                           | 2022-06-01        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2022-06-08        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2022-06-15        |
| RC testing                                                                                               | 2022-06-16..06-22 |
| v2023dev2 release                                                                                        | 2022-06-23        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
