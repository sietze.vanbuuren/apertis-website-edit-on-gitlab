#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright © 2016 Mathieu Duponchelle <mathieu.duponchelle@opencreed.com>
# Copyright © 2016 Collabora Ltd
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.

from lxml import etree
import errno
import lxml.html
import io
import os
import sys
import urlparse
import argparse
import tempfile
import subprocess

def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

def require_program(program):
    if which(program) is None:
        print "Checking for %s: no" % program
        print "Please install %s" % program
        sys.exit(1)

# Hotdoc outputs relative inter-document links.
# As these won't work with a pdf output, redirect these
# instead to the official web portal.
def update_local_links(elem, prefix):
    links = elem.xpath('.//a')
    for link in links:
        href = link.attrib.get('href')
        if href is None:
            continue
        url_components = urlparse.urlparse(href)

        if url_components.scheme or url_components.netloc:
            continue  # Not a local link

        if not url_components.path:
            continue  # Local link, but doesn't need updating (eg #my-anchor)

        link.attrib['href'] = '%s/%s' % (prefix, href)

def format_lntables(elem):
    tables = elem.xpath('.//table')
    for table in tables:
        cls = table.attrib.get('class')
        if cls != 'lntable':
            continue

        col = etree.Element("col")
        col.attrib['width'] = "97%"
        table.insert(0, col)
        col = etree.Element("col")
        col.attrib['width'] = "3%"
        table.insert(0, col)

def convert_images(elem, imgdir):
    imgs = elem.xpath('.//img')
    for img in imgs:
        src = img.attrib.get('src')
        if src is None:
            continue
        url_components = urlparse.urlparse(src)

        if url_components.scheme or url_components.netloc:
            continue  # Not a local url

        if not url_components.path:
            continue  # Local url, but doesn't need updating (eg #my-anchor)

        src = 'static' + os.path.abspath(src)
        converted = src

        if url_components.path.endswith('.svg'):
            converted = imgdir + "/" + os.path.basename(src) + '.pdf'
            cmd = ['/usr/bin/rsvg-convert', src, '-f', 'pdf', '-o', converted]

            subprocess.check_call(cmd)

        img.attrib['src'] = converted

def transform(args, tmpdir_path):
    with io.open(args.input, 'r', encoding='utf-8') as _:
        contents = _.read()

    root = etree.HTML(contents)

    output = []
    head = root.find(".//head")
    body = root.find(".//article")

    if head is None or body is None:
        print "Unexpected html"
        sys.exit(1)

    if args.remote_prefix:
        update_local_links(body, args.remote_prefix)

    convert_images(body, tmpdir_path)
    format_lntables(body)

    output.append('<!DOCTYPE html>')
    output.append('<html lang="en">')
    output.append(lxml.html.tostring(head, include_meta_content_type=True))
    output.append('<body>')
    output.append(lxml.html.tostring(body))
    output.append('</body>')
    output.append('</html>')

    return output

def write(args, output):
    program_dir = os.path.dirname(os.path.realpath(__file__))
    latex_dir = os.path.join(program_dir, "latex")

    # We need to stay in the same directory as the original html file
    # as pandoc doesn't know about assets paths otherwise
    inpath = os.path.dirname(args.input)
    os.chdir(inpath)

    tmp_html, tmp_path = tempfile.mkstemp(prefix=os.path.join(inpath, ''),
                                          suffix='.html')
    os.write(tmp_html, u'\n'.join(output))

    folder = os.path.dirname(args.output)
    try:
        os.makedirs(folder)
    except OSError, e:
        if e.errno != errno.EEXIST:
            raise e

    cmd = ['/usr/bin/pandoc', tmp_path, '-V documentclass=article',
           '--variable', 'urlcolor=NavyBlue',
           '--filter', os.path.join(program_dir, 'pandoc-add-link-footnotes.py'),
           '--pdf-engine=xelatex', '--toc', '-o', args.output,
           # because it's 2019 and the TeX engines still have no glyph fallback
           '--variable', 'CJKmainfont=Noto Serif CJK KR', # this one seems a fair bet
           '--variable', 'monofont=FreeMono', # has a good Unicode coverage without requiring fallbacks
           '--variable', 'monofontoptions=Scale=0.75', # make the very wide FreeMono font a bit smaller
           '--include-in-header='+os.path.join(latex_dir, 'apertis_logo.latex')]

    if args.line_numbers:
        cmd += ['--include-in-header='+os.path.join(latex_dir, 'linenumbers.latex')]

    status = subprocess.call(cmd)

    os.close(tmp_html)
    os.remove(tmp_path)

    return status

DESCRIPTION=\
'''
This program transforms html as output by hotdoc to
pdf, performing a few optimizations for that target format.
'''

if __name__=='__main__':
    # Basic sanity check.
    # If xelatex or relevant texlive packages are missing, pandoc will tell
    # the user.
    require_program('pandoc')

    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('input')
    parser.add_argument('output')
    parser.add_argument('--remote-prefix',
                        help='The url of the official web portal, if provided '
                             'local links will be prefixed with it.')
    parser.add_argument('--line-numbers',
                        action="store_true",
                        help='Add line numbers to the output document.')
    args = parser.parse_args()

    # We want to work with absolute paths
    args.input = os.path.abspath(args.input)
    args.output = os.path.abspath(args.output)

    inpath = os.path.dirname(args.input)
    tmpdir_path = tempfile.mkdtemp(prefix=os.path.join(inpath, ''))

    output = transform(args, tmpdir_path)
    status = write(args, output)

    files = os.listdir(tmpdir_path)
    for file in files:
        os.remove(tmpdir_path + "/" + file)
    os.rmdir(tmpdir_path)

    sys.exit(status)
